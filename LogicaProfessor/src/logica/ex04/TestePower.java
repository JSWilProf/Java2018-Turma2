package logica.ex04;

import br.senai.sp.info132.console.Programa;

public class TestePower extends Programa {
	@Override
	public void inicio() {
		int num1 = 3;
		int num2 = 2;
		
		double resultado = Math.pow(num1, num2);
		
		escrevaL("num1 elevado num2 : " , resultado);
	}
}
