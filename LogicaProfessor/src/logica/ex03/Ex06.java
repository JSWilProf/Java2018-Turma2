package logica.ex03;

import br.senai.sp.info132.console.Programa;

public class Ex06 extends Programa {
	@Override
	public void inicio() {
		int idade = leInteiro("Informe a sua idade");
		
		escreva("Sua Classificação é: ");
		
		if(idade <= 10) {
			escrevaL("Infantil");
		} else if(idade <= 15) {
			escrevaL("Infanto");
		} else if(idade <= 18) {
			escrevaL("Juvenil");
		} else {
			escrevaL("Adulto");
		}
	}
}
