package logica.ex05;

import br.senai.sp.info132.console.Programa;

public class Ex01 extends Programa {
	@Override
	public void inicio() {
		for (int num = 2; num <= 1000; num += 2) {
			escreva(String.format("%04d ", num));
		}
	}
}
