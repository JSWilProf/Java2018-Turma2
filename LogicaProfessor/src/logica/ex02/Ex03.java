package logica.ex02;

import br.senai.sp.info132.console.Programa;

public class Ex03 extends Programa {
	@Override
	public void inicio() {
		int saque = leInteiro("Informe o valor do Saque");

		escrevaL("Notas de 100: ", saque / 100);
		saque = saque % 100;
		escrevaL("Notas de 50: ", saque / 50);
		saque = saque % 50;
		escrevaL("Notas de 20: ", saque / 20);
		saque = saque % 20;
		escrevaL("Notas de 10: ", saque / 10);
		saque = saque % 10;
		escrevaL("Notas de 5: ", saque / 5);
		saque = saque % 5;
		escrevaL("Notas de 2: ", saque / 2);
		saque = saque % 2;
		escrevaL("Notas de 1: ", saque);
	}
}	
