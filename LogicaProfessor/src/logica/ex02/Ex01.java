package logica.ex02;

import br.senai.sp.info132.console.Programa;

public class Ex01 extends Programa {
	@Override
	public void inicio() {
		double largura = leReal("Informe a largura");
		double comprimento = leReal("Informe o comprimento");
		double profundidade = leReal("Informe a profundidade");
		escrevaL("O Preço final é de R$", largura * comprimento * profundidade * 45);
	}
}
