package logica.ex02;

import br.senai.sp.info132.console.Programa;

public class ExemploMaisMais extends Programa {
	@Override
	public void inicio() {
		int i = 10;
		int j = i++;
		escrevaL("Valor do I: ", i, " valor do j: ", j);		
		
		i = 10;
		int k = ++i;
		escrevaL("Valor do I: ", i, " valor do k: ", k);
	}
}
