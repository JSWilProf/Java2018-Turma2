package objetos.exemplos.agregacao;

import lombok.Data;

@Data
public class Atleta {
	private String nome;
	private int idade;
	private String posicao;
}
